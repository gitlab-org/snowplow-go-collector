This is a Snowplow collector that writes to PostgreSQL so we don't have to use Scale collector that needs the JVM and Kafka.
It is used by GitLab for Product Analytics.

## Start

    PA_NETWORK=unix \
    PA_USER=sytse \
    PA_DATABASE=gitlabhq_development \
    PA_ADDR=/Users/sytse/Dropbox/Repos/gitlab-development-kit/postgresql/.s.PGSQL.5432 \ 
    PA_PORT=:8080 \
    REDIS_SOCKET=/Users/sytse/Dropbox/Repos/gitlab-development-kit/redis/redis.socket
    go run main.go

## Test

    go test

When testing we don't write to the database because I couldn't make it work:

1. Most use a real database for testing: https://github.com/go-pg/pg/issues/959 but that doesn't work with AutoDevOps
1. I tried using https://gitlab.com/slax0rr/go-pg-wrapper but that didn't work with the Persistance struct expecting the DB.wrap in test.
1. I tried using a global variable instead of a struct but it seemed to segfault because of the connection pool not working with goroutines.

## Load test

It handles about 8000 events per second during testing with k6 using the goroutines of net/http when I test it with:

    k6 run --vus 50 --duration 5s k6_load_test.js

## Limits implemented in database

Requirements:

1. No extra write on every event (simple read on every event is OK)
1. No complex read on every event (can't do counting)
1. Want an explicit permanent record of hitting the limit (can't do in Redis)
1. If product analytics is not used it shouldn't use compute.
1. Should happen in the Go app

Function:

1. Before every write check if limited, read last item for that app_id not older than 10 seconds, proceed if not found or not limited
1. Write event
1. Pass if one in 10 event
1. Check if over 100 events for the last 10 seconds
1. Write the result, write in all cases, limited or not.

    table: product_analytics_limits

    app_id (integer)
    created_at (time)
    over (true/false)

    =>

Although reads are much more scalable than writes (can happen on a replication server) the potential read load is enourmous. We can't have it hit the database.

## Limits implemented in Redis

https://github.com/go-redis/redis_rate

Not sure how to detect rate limiting:

1. Calculate if there are too many events. (complex query, double work, rate per user might differ)
1. Write to database (when to write this, how to prevent many writes)
1. Delay solving this

## Install dependencies

    https://ahmadawais.com/install-go-lang-on-macos-with-homebrew/
    export GOPATH="${HOME}/.go"
    export GOROOT="$(brew --prefix golang)/libexec"
    export PATH="$PATH:${GOPATH}/bin:${GOROOT}/bin"
    test -d "${GOPATH}" || mkdir "${GOPATH}"
    test -d "${GOPATH}/src/github.com" || mkdir -p "${GOPATH}/src/github.com"
    export GOBIN=$GOROOT/bin

    go get
