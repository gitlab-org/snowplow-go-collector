module gitlab.com/gitlab-org/snowplow-go-collector

go 1.13

require (
	github.com/alicebob/gopher-json v0.0.0-20180125190556-5a6b3ba71ee6 // indirect
	github.com/alicebob/miniredis v2.5.0+incompatible
	github.com/go-pg/pg v8.0.6+incompatible
	github.com/go-redis/redis/v7 v7.2.0
	github.com/go-redis/redis_rate/v8 v8.0.0
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/yuin/gopher-lua v0.0.0-20191220021717-ab39c6098bdb // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	mellium.im/sasl v0.2.1 // indirect
)
