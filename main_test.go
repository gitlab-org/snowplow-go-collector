package main

import (
	"github.com/go-pg/pg"
	"net/http"
	"net/http/httptest"
	"testing"
  "github.com/alicebob/miniredis"
  "github.com/go-redis/redis/v7"
  "github.com/go-redis/redis_rate/v8"
)

func setup_test(t *testing.T) *http.Request {
  SetVersion()

  // Setup Redis and the limiter
  mr, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
  defer mr.Close()
  rdb := redis.NewClient(&redis.Options{
    Addr: mr.Addr(),
  })
  limiter = redis_rate.NewLimiter(rdb)

  // Setup the database, this fails but it doesn't matter.
  persistance := &Persistance{db: pg.Connect(&pg.Options{})}

	// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
	// pass 'nil' as the third parameter.
	req, err := http.NewRequest("GET", "/i?stm=1585427766447&e=pv&url=http%3A%2F%2Flocalhost%3A3000%2Fh5bp%2Fhtml5-boilerplate%2F-%2Fproduct_analytics%2Ftest&page=H5bp%20%2F%20Html5%20Boilerplate%20%C2%B7%20GitLab&refr=http%3A%2F%2Flocalhost%3A3000%2Fh5bp%2Fhtml5-boilerplate%2F-%2Fproduct_analytics%2Ftest&tv=js-2.14.0&tna=sp&aid=8&p=app&tz=America%2FLos_Angeles&lang=en-US&cs=UTF-8&f_pdf=1&f_qt=0&f_realp=0&f_wma=0&f_dir=0&f_fla=0&f_java=0&f_gears=0&f_ag=0&res=3008x1692&cd=24&cookie=1&eid=a6921333-d118-4b97-a7de-f8a27926c089&dtm=1585427766429&vp=616x1478&ds=601x1478&vid=4&sid=b01e1c4a-9bd4-415b-9ea7-ca19a89b68ef&duid=bb934f79-0492-4093-902f-21224240381c", nil)
	if err != nil {
		t.Fatal(err)
  }

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(persistance.HelloServer)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	return req
}

func TestIntFromParams(t *testing.T) {
	req := setup_test(t)
	wanted_result := 4
	if result := IntFromParams(req, "vid"); result != wanted_result {
		t.Errorf("handler returned wrong status code: got %v want %v",
			result, wanted_result)
	}
}

func TestStringFromParams(t *testing.T) {
	req := setup_test(t)
	wanted_result := "a6921333-d118-4b97-a7de-f8a27926c089"
	if result := StringFromParams(req, "eid"); result != wanted_result {
		t.Errorf("handler returned wrong status code: got %v but want %v",
			result, wanted_result)
	}
}
